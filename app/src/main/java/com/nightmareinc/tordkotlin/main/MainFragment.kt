package com.nightmareinc.tordkotlin.main

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.nightmareinc.tordkotlin.R
import com.nightmareinc.tordkotlin.databinding.FragmentMainBinding
import com.nightmareinc.tordkotlin.mole.MoleActivity
import com.nightmareinc.tordkotlin.puzzle.MainActivity

class MainFragment : Fragment() {

    lateinit var binding: FragmentMainBinding
    lateinit var mainViewModel: MainViewModel

    private lateinit var myList: ArrayList<MainModel>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_main, container, false
        )

        val viewModelFactory = MainViewModelFactory()
        mainViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)

        binding.mainViewModel = mainViewModel
        binding.lifecycleOwner = this

        val manager = GridLayoutManager(context, 2)
        binding.userList.layoutManager = manager

        val adapter = MainAdapter(MainListener { userId ->
            mainViewModel.onMainItemClicked(userId)
        })
        binding.userList.adapter = adapter

        // Fill main games item
        initMainItems()
        adapter.submitList(myList)

        // Handle navigation
        navigationToItems()

        return binding.root
    }

    private fun initMainItems() {
        myList = ArrayList<MainModel>()
        myList.add(0, MainModel(1001, "2048", R.drawable.logo_2048))
        myList.add(1, MainModel(1002, "Mole", R.drawable.mole))
        myList.add(2, MainModel(1003, "Minesweeper", R.drawable.logo_minesweeper))
        myList.add(3, MainModel(1003, "TorD", R.drawable.logo_tord))
    }

    private fun navigationToItems() {
        mainViewModel.navigateToMainItems.observe(this, Observer { item ->
            when (item) {
                "2048" -> {
                    val intent = Intent(activity, MainActivity::class.java)
                    startActivity(intent)
                }
                "Mole" -> {
                    val intent = Intent(activity, MoleActivity::class.java)
                    startActivity(intent)
                }
                "Minesweeper" -> {
                    this.findNavController().navigate(
                        MainFragmentDirections.actionMainFragmentToMinesweeperFragment()
                    )
                }
                "TorD" -> {
                    this.findNavController().navigate(
                        MainFragmentDirections.actionMainFragmentToCatFragment()
                    )
                }

            }
        })
    }

}