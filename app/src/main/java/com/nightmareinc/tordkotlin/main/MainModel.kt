package com.nightmareinc.tordkotlin.main

data class MainModel(
    var id: Int,
    var name: String,
    var image: Int
)