package com.nightmareinc.tordkotlin.main

import androidx.lifecycle.ViewModel
import com.nightmareinc.tordkotlin.util.SingleLiveData

class MainViewModel() : ViewModel() {

    val navigateToMainItems = SingleLiveData.SingleLiveEvent<String>()

    fun onMainItemClicked(id: String) {
        navigateToMainItems.value = id
    }

}