package com.nightmareinc.tordkotlin.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.nightmareinc.tordkotlin.databinding.ItemsMainBinding

class MainAdapter(val clickListener: MainListener) :
    ListAdapter<MainModel, MainAdapter.ViewHolder>(MainItemDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position)!!, clickListener)
    }

    class ViewHolder private constructor(val binding: ItemsMainBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: MainModel, clickListener: MainListener) {
            binding.mainItem = item
            binding.mainItemName.text = item.name
            binding.mainItemImage.setImageResource(item.image)
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemsMainBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }
}

class MainItemDiffCallback : DiffUtil.ItemCallback<MainModel>() {
    override fun areItemsTheSame(oldItem: MainModel, newItem: MainModel): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: MainModel, newItem: MainModel): Boolean {
        return oldItem == newItem
    }
}

class MainListener(val clickListener: (mainItemName: String) -> Unit) {
    fun onCLick(mainItem: MainModel) = clickListener(mainItem.name)
}