package com.nightmareinc.tordkotlin.minesweeper.game.generators

import com.nightmareinc.tordkotlin.minesweeper.game.Field

interface FieldGenerator {
    fun generate(rows: Int, columns: Int, args: FieldGenerationArguments): Field
}

data class FieldGenerationArguments(
    val mines: Int
)
