package com.nightmareinc.tordkotlin.minesweeper.game.moves

import com.nightmareinc.tordkotlin.minesweeper.game.Board

interface Move {
    enum class Type {
        Reveal,
        Flag,
        RemoveFlag
    }

    fun execute(board: Board, changeSet: Board.ChangeSet)
}
