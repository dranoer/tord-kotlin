package com.nightmareinc.tordkotlin.minesweeper.game.moves

import com.nightmareinc.tordkotlin.minesweeper.forEachEightNeighbor
import com.nightmareinc.tordkotlin.minesweeper.game.Board

class ChordMove(val row: Int, val column: Int) : Move {
    override fun execute(board: Board, changeSet: Board.ChangeSet) {
        if (!board.isRevealed(row, column)) return
        board.forEachEightNeighbor(row, column) { row, column ->
            if (!board.isFlagged(row, column)) changeSet.reveal(row, column)
        }
    }
}
