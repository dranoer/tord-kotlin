package com.nightmareinc.tordkotlin.tod.category

import androidx.lifecycle.ViewModel
import com.nightmareinc.tordkotlin.util.SingleLiveData

class CatViewModel () : ViewModel() {

    // Navigate to choose name screen
    val navigateToName = SingleLiveData.SingleLiveEvent<String>()

    fun onToDClicked(id: String?) {
        navigateToName.value = id
    }

}